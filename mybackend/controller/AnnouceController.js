const Annouce = require('../models/Annouce')
const annouceController = {
    async addAnnouce(req, res) {
        const payload = req.body
        const annouces = new Annouce(payload)
        try {
            await annouces.save()
            res.json(annouces)
        } catch (err) {
            res.status(500).send(err)
        }
    },
    async updateAnnouce(req, res) {
        const payload = req.body
        try {
            const annouces = await Annouce.updateOne({ _id: payload._id }, payload)
            res.json(annouces)
        } catch (err) {
            res.status(500).send(err)
        }
    },
    async deleteAnnouce(req, res) {
        const { id } = req.params
        try {
            const annouces = await Annouce.deleteOne({ _id: id })
            res.json(annouces)
        } catch (err) {
            res.status(500).send(err)
        }
    },
    async getAnnouce(req, res) {
        try {
            const annouces = await Annouce.find({})
            console.log(annouces)
            res.json(annouces)
        } catch (err) {
            res.status(500).send(err)
        }
    },
    async getAnnouceID(req, res) {
        const { id } = req.params

        try {
            const annouces = await Annouce.findById(id)
            res.json(annouces)
        } catch (err) {
            res.status(500).send(err)
        }
    }
}
module.exports = annouceController