const Company = require('../models/Company')
const companyController = {
  async addCompany (req, res) {
    const payload = req.body
    const companies = new Company(payload)
    try {
      await companies.save()
      res.json(companies)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateCompany (req, res) {
    const payload = req.body
    try {
      const companies = await Company.updateOne({ _id: payload._id }, payload)
      res.json(companies)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteCompany (req, res) {
    const { id } = req.params
    try {
      const companies = await Company.deleteOne({ _id: id })
      res.json(companies)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getCompany (req, res) {
    try {
      const companies = await Company.find({})
      res.json(companies)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getCompanyID (req, res) {
    const { id } = req.params

    try {
      const companies = await Company.findById(id)
      res.json(companies)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = companyController
