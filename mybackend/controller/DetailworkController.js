const Detailwork = require('../models/Detailwork.js')
const detailworkController = {
  async addDetailwork (req, res) {
    const payload = req.body
    const detailworks = new Detailwork(payload)
    try {
      await detailworks.save()
      res.json(detailworks)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateDetailwork (req, res) {
    const payload = req.body
    try {
      const detailworks = await Detailwork.updateOne({ _id: payload._id }, payload)
      res.json(detailworks)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteDetailwork (req, res) {
    const { id } = req.params
    try {
      const detailworks = await Detailwork.deleteOne({ _id: id })
      res.json(detailworks)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getDetailwork (req, res) {
    try {
      const detailworks = await Detailwork.find({})
      res.json(detailworks)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getDetailworkID (req, res) {
    const { id } = req.params

    try {
      const detailworks = await Detailwork.findById(id)
      res.json(detailworks)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = detailworkController
