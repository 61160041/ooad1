const RegisterCompany = require('../models/RegisterCompany.js')
const registerCompanyController = {
  async addRegister (req, res) {
    const payload = req.body
    const registersCompany = new RegisterCompany(payload)
    try {
      await registersCompany.save()
      res.json(registersCompany)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateRegister (req, res) {
    const payload = req.body
    try {
      const registersCompany = await RegisterCompany.updateOne({ _id: payload._id }, payload)
      res.json(registersCompany)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteRegister (req, res) {
    const { id } = req.params
    try {
      const registersCompany = await RegisterCompany.deleteOne({ _id: id })
      res.json(registersCompany)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getRegister (req, res) {
    try {
      const registersCompany = await RegisterCompany.find({})
      res.json(registersCompany)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getRegisterID (req, res) {
    const { id } = req.params

    try {
      const registersCompany = await RegisterCompany.findById(id)
      res.json(registersCompany)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = registerCompanyController
