const Register = require('../models/Registers.js')
const registerController = {
  async addRegister (req, res) {
    const payload = req.body
    const registers = new Register(payload)
    try {
      await registers.save()
      res.json(registers)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateRegister (req, res) {
    const payload = req.body
    try {
      const registers = await Register.updateOne({ _id: payload._id }, payload)
      res.json(registers)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteRegister (req, res) {
    const { id } = req.params
    try {
      const registers = await Register.deleteOne({ _id: id })
      res.json(registers)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getRegister (req, res) {
    try {
      const registers = await Register.find({})
      res.json(registers)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getRegisterID (req, res) {
    const { id } = req.params

    try {
      const registers = await Register.findById(id)
      res.json(registers)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = registerController
