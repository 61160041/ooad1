const replyResume = require('../models/Replyresume')
const replyresumeController = {
  async addReply (req, res) {
    const payload = req.body
    const replyResumes = new replyResume(payload)
    try {
      await replyResumes.save()
      res.json(replyResumes)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteReply (req, res) {
    const { id } = req.params
    try {
      const replyResumes = await replyResume.deleteOne({ _id: id })
      res.json(replyResumes)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getReply (req, res) {
    try {
      const replyResumes = await replyResume.find({})
      res.json(replyResumes)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getReplyID (req, res) {
    const { id } = req.params

    try {
      const replyResumes = await replyResume.findById(id)
      res.json(replyResumes)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = replyresumeController