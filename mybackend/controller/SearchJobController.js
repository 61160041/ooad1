const searchJob = require('../models/Annouce')
var position
var type
var recruitment
var working_time
var salary
var gender
var age
var education
var work_experience
var startDate
var endDate
var other_features
const searchJobController = {
  async getJob (eq, res) {
    try {
      const search = await searchJob.find({})
      res.json(search)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getJobPosition (req, res) {
    // var regex = new RegExp(req.params.position, 'i');
    position = req.params.position
    type = position
    gender = position
    age = position
    education = position
    recruitment = position
    working_time = position
    salary = position
    other_features = position
    work_experience = position
    startDate = position
    endDate = position
    try {
      searchJob
        .find({
          $or: [
            { position: new RegExp(position) },
            { type: new RegExp(type) },
            { gender: new RegExp(gender) },
            { age: new RegExp(age) },
            { recruitment: new RegExp(recruitment) },
            { working_time: new RegExp(working_time) },
            { salary: new RegExp(salary) },
            { education: new RegExp(education) },
            { work_experience: new RegExp(work_experience) },
            { startDate: new RegExp(startDate) },
            { endDate: new RegExp(endDate) },
            { other_features: new RegExp(other_features) }
          ]
        })
        .then(result => {
          res.json(result)
        })
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = searchJobController
