const mongoose = require('mongoose')
const annouceSchema = new mongoose.Schema({
  id: String,
  position: String,
  type: String,
  recruitment: String,
  working_time: String,
  salary: String,
  gender: String,
  age: String,
  education: String,
  work_experience: String,
  startDate: String,
  endDate: String,
  other_features: String
})

module.exports = mongoose.model('annouce', annouceSchema)
