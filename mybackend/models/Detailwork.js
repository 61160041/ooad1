const mongoose = require('mongoose')
const detailworkSchema = new mongoose.Schema({
    name: String,
    lastname: String,
    age: String,
    tel: String,
    email: String,
    nationality: String,
    education: String,
    languageSkill: String
})
module.exports = mongoose.model('detailwork', detailworkSchema)