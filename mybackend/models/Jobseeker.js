const mongoose = require('mongoose')
const jobseekerSchema = new mongoose.Schema({
  id: String,
  name: String,
  lastname: String,
  age: String,
  tel: String,
  email: String,
  nationality: String,
  religion: String,
  address: String,
  height: String,
  weight: String,
  status: String,
  birthdate: String
})
module.exports = mongoose.model('jobseeker', jobseekerSchema)
