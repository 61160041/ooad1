const mongoose = require('mongoose')
const Schema = mongoose.Schema
const registerSchema = new Schema({
  id: String,
  email: String,
  password: String,
  name: String,
  surname: String,
  address: String,
  province: String,
  district: String,
  subarea: String,
  zip: String,
  tel: String,
  typeRegister: String
  
})
module.exports = mongoose.model('register', registerSchema)
