const mongoose = require('mongoose')
const replySchema = new mongoose.Schema({
  id: String,
  company: String,
  name: String,
  status: String
})
module.exports = mongoose.model('reply', replySchema)
