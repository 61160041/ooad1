const mongoose = require('mongoose')
const Schema = mongoose.Schema

const companySchema = new Schema({
  companyName: String,
  contactName: String
})
module.exports = mongoose.model('Company', companySchema)
