const express = require('express')
const router = express.Router()
const annouceController = require('../controller/AnnouceController')
router.get('/',annouceController.getAnnouce)
router.get('/:id',annouceController.getAnnouceID)
router.post('/',annouceController.addAnnouce)
router.put('/',annouceController.updateAnnouce)
router.delete('/:id',annouceController.deleteAnnouce)
module.exports = router