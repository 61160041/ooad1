const express = require('express')
const router = express.Router()
const companyController = require('../controller/CompanyController')
router.get('/',companyController.getCompany)
router.get('/:id',companyController.getCompanyID)
router.post('/',companyController.addCompany)
router.put('/',companyController.updateCompany)
router.delete('/:id',companyController.deleteCompany)
module.exports = router