const express = require('express')
const router = express.Router()
const detailworkController = require('../controller/DetailworkController.js')
router.get('/',detailworkController.getDetailwork)
router.get('/:id',detailworkController.getDetailworkID)
router.post('/',detailworkController.addDetailwork)
router.put('/',detailworkController.updateDetailwork)
router.delete('/:id',detailworkController.deleteDetailwork)
module.exports = router