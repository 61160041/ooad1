const express = require('express')
const router = express.Router()
const detailworkController = require('../controller/JobseekerController')
router.get('/', detailworkController.getJobseeker)
router.get('/:id', detailworkController.getJobseekerID)
router.post('/', detailworkController.addJobseeker)
router.put('/', detailworkController.updateJobseeker)
router.delete('/:id', detailworkController.deleteJobseeker)
module.exports = router
