const express = require('express')
const router = express.Router()
const registerController = require('../controller/RegisterController.js')
const Login = require('../models/Registers')

router.get('/', registerController.getRegister)
router.get('/:id', registerController.getRegisterID)
router.post('/', registerController.addRegister)
router.post('/checkLogin', async (req, res, next) => {
  const payload = req.body
  console.log(payload)

  Login.findOne(payload).then(function (login) {
    res.json(login)
    console.log(login)
  }).catch(function (err) {
    res.status(500).send(err)
  })

})
router.put('/', registerController.updateRegister)
router.delete('/:id', registerController.deleteRegister)

module.exports = router
