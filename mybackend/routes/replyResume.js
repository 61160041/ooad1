const express = require('express')
const router = express.Router()
const replyController = require('../controller/ReplyResumeController')
router.get('/',replyController.getReply)
router.get('/:id',replyController.getReplyID)
router.post('/',replyController.addReply)
router.delete('/:id',replyController.deleteReply)
module.exports = router