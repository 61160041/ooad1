const express = require('express')
const router = express.Router()
const searchJobController = require('../controller/SearchJobController')
router.get('/',searchJobController.getJob)
router.get('/:position',searchJobController.getJobPosition)
module.exports = router