import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/users/',
    name: 'users',
    component: () => import('../views/Users/resume.vue')
  },
  {
    path: '/FastJob/',
    name: 'FastJob',
    component: () => import('../views/FastJob.vue')
  },
  {
    path: '/annouce/',
    name: 'annouce',
    component: () => import('../views/AnnounceWork.vue')
  },

  {
    path: '/Parttime&Work/',
    name: 'Parttime&Work',
    component: () => import('../views/Parttime&Work.vue')
  },
  {
    path: '/News/',
    name: 'News',
    component: () => import('../views/News.vue')
  },
  {
    path: '/AboutMe/',
    name: 'AboutMe',
    component: () => import('../views/AboutMe.vue')
  },
  {
    path: '/company/',
    name: 'company',
    component: () => import('../views/Company/company.vue')
  },

  {
    path: '/checkResume/',
    name: 'checkResume',
    component: () => import('../views/Company/checkResume.vue')
  },

  {
    path: '/jobseeker/',
    name: 'jobseeker',
    component: () => import('../views/JobSeeker/jobseeker.vue')
  },
  {
    path: '/register/',
    name: 'register',
    component: () => import('../views/Register.vue')
  },
  {
    path: '/Detail/',
    name: 'Detail',
    component: () => import('../views/Detail.vue')
  },
  {
    path: '/Search/',
    name: 'Search',
    component: () => import('../views/Search.vue')
  },
  {
    path: '/detailResume/',
    name: 'detailResume',
    component: () => import('../views/Company/detailResume.vue')
  },
  {
    path: '/profile/',
    name: 'profile',
    component: () => import('../views/profile.vue')
  },
  {
    path: '/replyResume/',
    name: 'replyResume',
    component: () => import('../views/replyResume.vue')
  },
  {
    path: '/login/',
    name: 'replloginyResume',
    component: () => import('../views/login.vue')
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
